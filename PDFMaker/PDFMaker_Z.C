#include "TROOT.h"
#include "TFile.h"
#include "TStyle.h"
#include "TCanvas.h"
#include "TTree.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TMath.h"
#include "TString.h"
#include "TColor.h"
#include "TLegend.h"

#include <iostream>

#include "setup.C"

using namespace std;
//using namespace std::literals::string_literals;

//|**main function
//*This function makes pdf file of histgrams which is made by CalcEffPlotMakerOrigin.
//*For using this function, you must choose 7 parameters (actually outputPDF is depend on other 6 parameters).
//*Input file is root file which is made by CalcEffPlotMakerOrigin.
//*To use this function, use *.sh e.q. "$./test1107.sh test_jptap_all.root " .
void PDFMaker_Z(const char * input, const char * outputPDF,const char * jpsiz, const char * kind, const char * dimension, const char * variable,const char * trigger, const char * chain){


  //prepare array for histgram. When adding other parameters, need to add it to this list.
  //Be careful to forget to separating 1D, 2D, 3D.
  //
  std::vector<std::string> kind_array;
  kind_array.push_back("eff");
  kind_array.push_back("probe");

  std::vector<std::string> variable_array;
  variable_array.push_back("mass");
  variable_array.push_back("pt");
  variable_array.push_back("eta");
  variable_array.push_back("phi");
  variable_array.push_back("dR");
  variable_array.push_back("exdR");
  variable_array.push_back("d0");
  variable_array.push_back("z0");
  variable_array.push_back("mdR"); 

  std::vector<std::string> variable_array_2D;
  variable_array_2D.push_back("qetapt");
  variable_array_2D.push_back("qetaphi");
  variable_array_2D.push_back("etaphi");
  variable_array_2D.push_back("phipt");
  variable_array_2D.push_back("dRpt");
  variable_array_2D.push_back("exdRpt");
  variable_array_2D.push_back("exdRdR"); 

  std::vector<std::string> trigger_array;
  trigger_array.push_back("mu4");
  trigger_array.push_back("mu6");
  trigger_array.push_back("mu6ms");
  trigger_array.push_back("mu10");
  trigger_array.push_back("mu10nc");
  trigger_array.push_back("mu11");
  trigger_array.push_back("mu11nc");
  trigger_array.push_back("mu14");
  trigger_array.push_back("mu18");
  trigger_array.push_back("mu20");
  trigger_array.push_back("mu20ms");
  trigger_array.push_back("mu24im");
  trigger_array.push_back("mu26im");
  trigger_array.push_back("mu26ivm");
  trigger_array.push_back("mu50");
  trigger_array.push_back("mu60");
  trigger_array.push_back("mu4FS");
  trigger_array.push_back("mu4FSMU6");
  trigger_array.push_back("mu6FS");
  trigger_array.push_back("mu6FSMU6");
  trigger_array.push_back("mu8FS");
  trigger_array.push_back("2mu6");
  trigger_array.push_back("2mu10");
  trigger_array.push_back("2mu10nocb");
  trigger_array.push_back("mu11mu6");

  std::vector<std::string> chain_eff_array;
  chain_eff_array.push_back("L1");
  chain_eff_array.push_back("L1SA");
  chain_eff_array.push_back("SACB");
  chain_eff_array.push_back("L2");
  chain_eff_array.push_back("L2_2");
  chain_eff_array.push_back("L2EF");
  chain_eff_array.push_back("TOTAL");
  chain_eff_array.push_back("FTF");
  chain_eff_array.push_back("SAFTF");
  chain_eff_array.push_back("L1L2");
  chain_eff_array.push_back("HLT");
  chain_eff_array.push_back("TOTAL_2");
  chain_eff_array.push_back("HLT_2");//what is L2_2 

  std::vector<std::string> chain_probe_array;
  chain_probe_array.push_back("PROBE");
  chain_probe_array.push_back("L1");
  chain_probe_array.push_back("SA");
  chain_probe_array.push_back("CB");
  chain_probe_array.push_back("FTF");
  chain_probe_array.push_back("FTF_2");
  chain_probe_array.push_back("PROBEDUMMY");
  chain_probe_array.push_back("L2DUMMY");
  chain_probe_array.push_back("EF"); 


  //prepare plot; to use 256 colors.
  gROOT->SetStyle("Plain");
  gStyle->SetOptStat(1);
  //gStyle->SetPalette(1);
  gStyle -> SetNumberContours(100);
  const Int_t NRGBs = 5;
  Double_t stops[NRGBs] = { 0.00, 0.34, 0.61, 0.84, 1.00 };
  Double_t red[NRGBs]   = { 0.00, 0.00, 0.87, 1.00, 0.51 };
  Double_t green[NRGBs] = { 0.00, 0.81, 1.00, 0.20, 0.00 };
  Double_t blue[NRGBs]  = { 0.51, 1.00, 0.12, 0.00, 0.00 };
  TColor::CreateGradientColorTable(NRGBs, stops, red, green, blue, 256);


  //Load file; input is function parameters.
  TFile *file = new TFile(input);

  //[variables]const char * --->>> std::stgring; Function parameters is type of const char, so must change const char to string, because of using "(TFile*)file ->Get()".

  std::string ki = std::string(kind);
  std::string jz = std::string(jpsiz);
  std::string va = std::string(variable);
  std::string tr = std::string(trigger);
  std::string ch = std::string(chain);
  std::string di = std::string(dimension);
  std::string output = jz+"_"+ki+"_"+"*_"+tr+"_"+ch;


  //==========if variable == ALL=============
  //This is make h_*d_*_trriger_chain histgrams and make pdf ( * is the wildcard.) .
  //need to separate 1D, 2D, 3D
  //
  //Define canvas
  TString name_va;
  name_va.Form(outputPDF);
  TCanvas *c_va = new TCanvas(name_va.Data(), name_va.Data(), 1000, 500);
  c_va->SetLeftMargin(0.25);
  c_va->SetRightMargin(0.25);
  c_va->SetBottomMargin(0.20);
  c_va->SetTopMargin(0.20);

  TH1F* h_va_1D[10]; 
  TH2F* h_va_2D[10];  
  std::vector<std::string> h_variable;
  std::vector<TString> TS_va_hist;
  Int_t MAX_va =0;

  //8,9,6 is number of variable_array for each case(1D, 2D, 3D).
  if(va == "ALL"){
    if(ki == "eff" && di == "1d"){
      MAX_va = 8;
    }else if(ki == "probe" && di == "1d"){
      MAX_va = 9;
    }else if(di == "2d"){
      MAX_va = 6;
    }
    //  cout << MAX_va << endl;
    c_va -> Print(name_va + "[", "pdf");  
    for(int n_va = 0;n_va<MAX_va;++n_va){ 
      if(di == "1d"){
        h_variable.push_back("h_"+ki+"_"+variable_array[n_va]+"_"+tr+"_"+ch);
        TS_va_hist.push_back(h_variable[n_va]);
        cout << "======================= [" << n_va << "] : " << "[" <<  di << "]" << " : " <<  TS_va_hist[n_va] << "===================" << endl;
        h_va_1D[n_va] = (TH1F*)file ->Get(TS_va_hist[n_va]);
        h_va_1D[n_va] -> SetTitle(TS_va_hist[n_va]);
        SetgPad();
        h_va_1D[n_va] -> Draw("COLZ");
      }
      if(di == "2d"){
        h_variable.push_back("h_"+ki+"_"+variable_array_2D[n_va]+"_"+tr+"_"+ch);
        TS_va_hist.push_back(h_variable[n_va]);
        cout << "======================= [" << n_va << "] : " << "[" <<  di << "]" << " : " <<  TS_va_hist[n_va] << "===================" << endl;
        h_va_2D[n_va] = (TH2F*)file ->Get(TS_va_hist[n_va]);
        h_va_2D[n_va] -> SetTitle(TS_va_hist[n_va]);
        h_va_2D[n_va] -> GetXaxis() -> SetRangeUser(-2.4,2.4);
        SetgPad();
        h_va_2D[n_va] -> Draw("COLZ");
      }
      c_va->Print(name_va , "pdf");
    }
    c_va -> Print(name_va + "]" , "pdf");
  }

  //==========if trigger == ALL=============
  //This is make h_*d_variable_*_chain histgrams and make pdf.( * is the wildcard.) .
  //need to separate 1D, 2D, 3D
  //Define canvas
  TString name_tr;
  name_tr.Form(outputPDF);
  TCanvas *c_tr = new TCanvas(name_tr.Data(), name_tr.Data(), 1000, 500);
  c_tr->SetLeftMargin(0.25);
  c_tr->SetRightMargin(0.25);
  c_tr->SetBottomMargin(0.20);
  c_tr->SetTopMargin(0.20);

  TH2F* h_tr_2D[25];
  std::vector<TString> TS_tr_hist;
  
  TH1F* h_tr_L1_1D[25];
  std::vector<std::string> h_trigger_L1;
  std::vector<TString> TS_tr_hist_L1;
///when jpsi, need HLT_2 to get L2/PROBE.
///when z, need HLT? to get L2/PROBE.
  TH1F* h_tr_L2_1D[25];
  std::vector<std::string> h_trigger_L2;
  std::vector<TString> TS_tr_hist_L2;

  TH1F* h_tr_HLT_1D[25];
  std::vector<std::string> h_trigger_HLT;
  std::vector<TString> TS_tr_hist_HLT;


  TH1F* h_tr_TOTAL_1D[25];
  std::vector<std::string> h_trigger_TOTAL;
  std::vector<TString> TS_tr_hist_TOTAL;

  TH1F* h_tr_L1SA_1D[25];
  std::vector<std::string> h_trigger_L1SA;
  std::vector<TString> TS_tr_hist_L1SA;

  TH1F* h_tr_SACB_1D[25];
  std::vector<std::string> h_trigger_SACB;
  std::vector<TString> TS_tr_hist_SACB;

  TH1F* h_tr_L2EF_1D[25];
  std::vector<std::string> h_trigger_L2EF;
  std::vector<TString> TS_tr_hist_L2EF;

  std::vector<std::string> h_name;
  std::vector<TString> TS_tr_name;
  Int_t MAX_tr;
  MAX_tr = 24;

//  ch = "L1";

  if(tr == "ALL"){
    c_tr -> Print(name_tr + "[", "pdf");  
    for(int n_tr = 0;n_tr<MAX_tr;++n_tr){ 
      h_name.push_back("efficiency:" + trigger_array[n_tr]); 
      TS_tr_name.push_back(h_name[n_tr]);
      TLegend *leg1 = new TLegend(0.6,0.2,0.82,0.4,TS_tr_name[n_tr]);
      TLegend *leg2 = new TLegend(0.6,0.2,0.82,0.4,TS_tr_name[n_tr]);

      h_trigger_L1.push_back("h_"+ki+"_"+va+"_"+trigger_array[n_tr]+"_"+"L1");
      TS_tr_hist_L1.push_back(h_trigger_L1[n_tr]);
      
      h_trigger_L2.push_back("h_"+ki+"_"+va+"_"+trigger_array[n_tr]+"_"+"L2");
      TS_tr_hist_L2.push_back(h_trigger_L2[n_tr]);

      h_trigger_HLT.push_back("h_"+ki+"_"+va+"_"+trigger_array[n_tr]+"_"+"HLT");
      TS_tr_hist_HLT.push_back(h_trigger_HLT[n_tr]);

      h_trigger_TOTAL.push_back("h_"+ki+"_"+va+"_"+trigger_array[n_tr]+"_"+"TOTAL");
      TS_tr_hist_TOTAL.push_back(h_trigger_TOTAL[n_tr]);

      h_trigger_L1SA.push_back("h_"+ki+"_"+va+"_"+trigger_array[n_tr]+"_"+"L1SA");
      TS_tr_hist_L1SA.push_back(h_trigger_L1SA[n_tr]);
      
      h_trigger_SACB.push_back("h_"+ki+"_"+va+"_"+trigger_array[n_tr]+"_"+"SACB");
      TS_tr_hist_SACB.push_back(h_trigger_SACB[n_tr]);

      h_trigger_L2EF.push_back("h_"+ki+"_"+va+"_"+trigger_array[n_tr]+"_"+"L2EF");
      TS_tr_hist_L2EF.push_back(h_trigger_L2EF[n_tr]);

      //first page
      cout << "=======================[" << n_tr << "]" <<  TS_tr_hist_L1[n_tr] << "===================" << endl;
      if(di == "1d"){
        h_tr_L1_1D[n_tr] = (TH1F*)file ->Get(TS_tr_hist_L1[n_tr]);
//        h_tr_L1_1D[n_tr] -> SetTitle(TS_tr_hist_L1[n_tr]);
        h_tr_L1_1D[n_tr] -> SetMarkerColor(1);
        h_tr_L1_1D[n_tr] -> SetFillColor(1);
        h_tr_L1_1D[n_tr] -> SetLineColor(1);
        h_tr_L1_1D[n_tr] -> GetYaxis() -> SetRangeUser(0.,1.1);
        SetgPad();
        h_tr_L1_1D[n_tr] -> Draw();
        leg1 -> AddEntry(TS_tr_hist_L1[n_tr],"L1/PROBE","lep");

      cout << "=======================[" << n_tr << "]" <<  TS_tr_hist_L2[n_tr] << "===================" << endl;
        h_tr_L2_1D[n_tr] = (TH1F*)file ->Get(TS_tr_hist_L2[n_tr]);
 //       h_tr_L2_1D[n_tr] -> SetTitle(TS_tr_hist_L2[n_tr]);
        h_tr_L2_1D[n_tr] -> SetMarkerColor(2);
        h_tr_L2_1D[n_tr] -> SetFillColor(2);
        h_tr_L2_1D[n_tr] -> SetLineColor(2);
        h_tr_L2_1D[n_tr] -> GetYaxis() -> SetRangeUser(0.,1.1);
        SetgPad();
        h_tr_L2_1D[n_tr] -> Draw("SAME");
        leg1 -> AddEntry(TS_tr_hist_L2[n_tr],"L2/PROBE","lep");

        /*
      cout << "=======================[" << n_tr << "]" <<  TS_tr_hist_HLT[n_tr] << "===================" << endl;
        h_tr_HLT_1D[n_tr] = (TH1F*)file ->Get(TS_tr_hist_HLT[n_tr]);
 //       h_tr_SACB_1D[n_tr] -> SetTitle(TS_tr_hist_SACB[n_tr]);
        h_tr_HLT_1D[n_tr] -> SetMarkerColor(3);
        h_tr_HLT_1D[n_tr] -> SetFillColor(3);
        h_tr_HLT_1D[n_tr] -> SetLineColor(3);
        h_tr_HLT_1D[n_tr] -> GetYaxis() -> SetRangeUser(0.,1.1);
        h_tr_HLT_1D[n_tr] -> Draw("SAME");
        leg1 -> AddEntry(TS_tr_hist_HLT[n_tr],"HLT/PROBE","lep");
        */

      cout << "=======================[" << n_tr << "]" <<  TS_tr_hist_TOTAL[n_tr] << "===================" << endl;
        h_tr_TOTAL_1D[n_tr] = (TH1F*)file ->Get(TS_tr_hist_TOTAL[n_tr]);
 //       h_tr_SACB_1D[n_tr] -> SetTitle(TS_tr_hist_SACB[n_tr]);
        h_tr_TOTAL_1D[n_tr] -> SetMarkerColor(4);
        h_tr_TOTAL_1D[n_tr] -> SetFillColor(4);
        h_tr_TOTAL_1D[n_tr] -> SetLineColor(4);
        h_tr_TOTAL_1D[n_tr] -> GetYaxis() -> SetRangeUser(0.,1.1);
        SetgPad();
        h_tr_TOTAL_1D[n_tr] -> Draw("SAME");
        leg1 -> AddEntry(TS_tr_hist_TOTAL[n_tr],"EF/PROBE","lep");
        leg1 -> Draw();

        //next page
        c_tr->Print(name_tr , "pdf");
      cout << "=======================[" << n_tr << "]" <<  TS_tr_hist_L1[n_tr] << "===================" << endl;
        h_tr_L1_1D[n_tr] = (TH1F*)file ->Get(TS_tr_hist_L1[n_tr]);
//        h_tr_L1SA_1D[n_tr] -> SetTitle(TS_tr_hist_L1SA[n_tr]);
        h_tr_L1_1D[n_tr] -> SetMarkerColor(3);
        h_tr_L1_1D[n_tr] -> SetFillColor(3);
        h_tr_L1_1D[n_tr] -> SetLineColor(3);
        h_tr_L1_1D[n_tr] -> GetYaxis() -> SetRangeUser(0.,1.1);
        SetgPad();
        h_tr_L1_1D[n_tr] -> Draw();
        leg2 -> AddEntry(TS_tr_hist_L1[n_tr],"L1","lep");
        c_tr->Print(name_tr , "pdf");
      cout << "=======================[" << n_tr << "]" <<  TS_tr_hist_L1SA[n_tr] << "===================" << endl;
        h_tr_L1SA_1D[n_tr] = (TH1F*)file ->Get(TS_tr_hist_L1SA[n_tr]);
//        h_tr_L1SA_1D[n_tr] -> SetTitle(TS_tr_hist_L1SA[n_tr]);
        h_tr_L1SA_1D[n_tr] -> SetMarkerColor(2);
        h_tr_L1SA_1D[n_tr] -> SetFillColor(2);
        h_tr_L1SA_1D[n_tr] -> SetLineColor(2);
        h_tr_L1SA_1D[n_tr] -> GetYaxis() -> SetRangeUser(0.,1.1);
        SetgPad();
        h_tr_L1SA_1D[n_tr] -> Draw("SAME");
        leg2 -> AddEntry(TS_tr_hist_L1SA[n_tr],"SA/L1","lep");

      cout << "=======================[" << n_tr << "]" <<  TS_tr_hist_SACB[n_tr] << "===================" << endl;
        h_tr_SACB_1D[n_tr] = (TH1F*)file ->Get(TS_tr_hist_SACB[n_tr]);
 //       h_tr_SACB_1D[n_tr] -> SetTitle(TS_tr_hist_SACB[n_tr]);
        h_tr_SACB_1D[n_tr] -> SetMarkerColor(4);
        h_tr_SACB_1D[n_tr] -> SetFillColor(4);
        h_tr_SACB_1D[n_tr] -> SetLineColor(4);
        h_tr_SACB_1D[n_tr] -> GetYaxis() -> SetRangeUser(0.,1.1);
        SetgPad();
        h_tr_SACB_1D[n_tr] -> Draw("SAME");
        leg2 -> AddEntry(TS_tr_hist_SACB[n_tr],"CB/SA","lep");

       cout << "=======================[" << n_tr << "]" <<  TS_tr_hist_L2EF[n_tr] << "===================" << endl;
        h_tr_L2EF_1D[n_tr] = (TH1F*)file ->Get(TS_tr_hist_L2EF[n_tr]);
 //       h_tr_SACB_1D[n_tr] -> SetTitle(TS_tr_hist_SACB[n_tr]);
        h_tr_L2EF_1D[n_tr] -> SetMarkerColor(1);
        h_tr_L2EF_1D[n_tr] -> SetFillColor(1);
        h_tr_L2EF_1D[n_tr] -> SetLineColor(1);
        h_tr_L2EF_1D[n_tr] -> GetYaxis() -> SetRangeUser(0.,1.1);
        SetgPad();
        h_tr_L2EF_1D[n_tr] -> Draw("SAME");
        leg2 -> AddEntry(TS_tr_hist_L2EF[n_tr],"EF/CB","lep");
     

        leg2 -> Draw();
        c_tr->Print(name_tr , "pdf");
 
        //delete leg;
      }
      if(di == "2d"){
        h_tr_2D[n_tr] = (TH2F*)file ->Get(TS_tr_hist[n_tr]);
        h_tr_2D[n_tr] -> SetTitle(TS_tr_hist[n_tr]);
        h_tr_2D[n_tr] -> GetZaxis() -> SetRangeUser(0.,1.0);
        h_va_2D[n_tr] -> GetXaxis() -> SetRangeUser(-2.4,2.4);
        SetgPad();
        //SetHist2D(h_tr_2D[n_tr]);
        h_tr_2D[n_tr] -> Draw("COLZ");
        c_tr->Print(name_tr , "pdf");
      }
    }
    c_tr -> Print(name_tr + "]" , "pdf");
  }

  //==========if chain  == ALL=============
  //This is make h_*d_variable_trigger_* histgrams and make pdf.( * is the wildcard.) .
  //need to separate 1D, 2D, 3D
  //Define canvas
  TString name_ch;
  name_ch.Form(outputPDF);
  TCanvas *c_ch = new TCanvas(name_ch.Data(), name_ch.Data(), 1000, 500);
  c_ch->SetLeftMargin(0.25);
  c_ch->SetRightMargin(0.25);
  c_ch->SetBottomMargin(0.20);
  c_ch->SetTopMargin(0.20);

  TH1F* h_ch_1D[25];
  TH2F* h_ch_2D[25]; 
  std::vector<std::string> h_chain;
  std::vector<TString> TS_ch_hist;
  Int_t MAX_ch;

  if(ch == "ALL"){
    if(ki == "eff"){
      MAX_ch = 13;
    }else if(ki == "probe"){
      MAX_ch = 9;
    }

    c_ch -> Print(name_ch + "[", "pdf");  
    for(int n_ch = 0;n_ch<MAX_ch;++n_ch){ 
      if(ki == "eff"){
        h_chain.push_back("h_"+ki+"_"+va+"_"+tr+"_"+chain_eff_array[n_ch]);
      }
      if(ki == "probe"){
        h_chain.push_back("h_"+ki+"_"+va+"_"+tr+"_"+chain_probe_array[n_ch]);
      }
      TS_ch_hist.push_back(h_chain[n_ch]);
      cout << "=======================[" << n_ch << "]" <<  TS_ch_hist[n_ch] << "===================" << endl;
      if(di == "1d"){
        h_ch_1D[n_ch] = (TH1F*)file ->Get(TS_ch_hist[n_ch]);
        h_ch_1D[n_ch] -> SetTitle(TS_ch_hist[n_ch]);
        SetgPad();
        h_ch_1D[n_ch] -> Draw("COLZ");
        c_ch->Print(name_ch , "pdf");
      }

      if(di == "2d"){
        h_ch_2D[n_ch] = (TH2F*)file ->Get(TS_ch_hist[n_ch]);
        h_ch_2D[n_ch] -> SetTitle(TS_ch_hist[n_ch]);
        h_ch_2D[n_ch] -> GetZaxis() -> SetRangeUser(0.,1.0);
        SetgPad();
        h_ch_2D[n_ch] -> GetXaxis() -> SetRangeUser(-2.4,2.4);
        h_ch_2D[n_ch] -> Draw("COLZ");
        c_ch->Print(name_ch , "pdf");
      }
    }
    c_ch -> Print(name_ch + "]" , "pdf");
  }






  gROOT -> ProcessLine(".q"); 




}
