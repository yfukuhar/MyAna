#! /bin/bash
INPUT_ROOT_FILE=$1
OUTPUT_PDF_DIR="output_${INPUT_ROOT_FILE}/"

EFF="eff"
PROBE="probe"
HIST_D1="1d"
HIST_D2="2d"

echo "Base output directory: $OUTPUT_PDF_DIR"

if [ ! -d "$OUTPUT_PDF_DIR" ] ;then
  echo "===============Create ${OUTPUT_PDF_DIR} ===========" && mkdir $OUTPUT_PDF_DIR
fi


echo "******************************************************************"
echo "******************************************************************"
echo "[0]JPZ, [1]Data Type, [2]Label"
echo "******************************************************************"
echo "******************************************************************"

echo "[0]choose jp or z :"
read JPZ
echo ">>>>>>>>>>>>>>>>>>>>>>>"
echo ">>>>>>JPZ = ${JPZ}>>>>>>>"
echo ">>>>>>>>>>>>>>>>>>>>>>>"
echo ""

echo "[1]choose \"mc16\" or \"mc15\" or \"data16\" or \"data15\" :"
read DATA
echo ">>>>>>>>>>>>>>>>>>>>>>>"
echo ">>>>>>Data Type = ${DATA}>>>>>>>"
echo ">>>>>>>>>>>>>>>>>>>>>>>"

echo "[2]choose pdf label (all_AllPeriod, barrel_AllPeriod, endcap_AllPeriod)):"
read LABEL 
echo ">>>>>>>>>>>>>>>>>>>>>>>"
echo ">>>>>>Label = ${LABEL}>>>>>>>"
echo ">>>>>>>>>>>>>>>>>>>>>>>"

JPSIZ=${JPZ}${DATA}
echo "******************************"
echo ">>>>>>Input Type = ${JPSIZ}>>>>>>>"
echo ">>>>>>Label      = ${LABEL}>>>>>>>"
echo "******************************"
echo "******************************"


####JPsi mu4

#Jpsi 1d pt comparison
if [ ${JPSIZ} = "jpmc15" -o ${JPSIZ} = "jpmc16" -o ${JPSIZ} = "jpdata16" ];then
  OUTPUT_PDF_FILE="${JPSIZ}_h_1d_eff_pt_ALL_compare_${LABEL}.pdf"
  root -l -b "PDFMaker_JPsi.C++(\"$INPUT_ROOT_FILE\",\"$OUTPUT_PDF_FILE\",\"$JPSIZ\",\"eff\",\"1d\",\"pt\",\"ALL\",\"SELECT\")"
  mv $OUTPUT_PDF_FILE $OUTPUT_PDF_DIR  
fi

#Jpsi mu4 1d pt
if [ ${JPSIZ} = "jpmc15" -o ${JPSIZ} = "jpmc16" -o ${JPSIZ} = "jpdata16" ];then
  OUTPUT_PDF_FILE="${JPSIZ}_h_1d_eff_pt_mu4_ALL_${LABEL}.pdf"
  root -l -b "PDFMaker_JPsi.C++(\"$INPUT_ROOT_FILE\",\"$OUTPUT_PDF_FILE\",\"$JPSIZ\",\"eff\",\"1d\",\"pt\",\"mu4\",\"ALL\")"
  mv $OUTPUT_PDF_FILE $OUTPUT_PDF_DIR  
fi

#Jpsi mu4 1d eta
if [ ${JPSIZ} = "jpmc15" -o ${JPSIZ} = "jpmc16" -o ${JPSIZ} = "jpdata16" ];then
  OUTPUT_PDF_FILE="${JPSIZ}_h_1d_eff_eta_mu4_ALL_${LABEL}.pdf"
  root -l -b "PDFMaker_JPsi.C++(\"$INPUT_ROOT_FILE\",\"$OUTPUT_PDF_FILE\",\"$JPSIZ\",\"eff\",\"1d\",\"eta\",\"mu4\",\"ALL\")"
  mv $OUTPUT_PDF_FILE $OUTPUT_PDF_DIR  
fi

#Jpsi mu4 1d phi
if [ ${JPSIZ} = "jpmc15" -o ${JPSIZ} = "jpmc16" -o ${JPSIZ} = "jpdata16" ];then
  OUTPUT_PDF_FILE="${JPSIZ}_h_1d_eff_phi_mu4_ALL_${LABEL}.pdf"
  root -l -b "PDFMaker_JPsi.C++(\"$INPUT_ROOT_FILE\",\"$OUTPUT_PDF_FILE\",\"$JPSIZ\",\"eff\",\"1d\",\"phi\",\"mu4\",\"ALL\")"
  mv $OUTPUT_PDF_FILE $OUTPUT_PDF_DIR  
fi

#Jpsi mu4 2d etaphi
if [ ${JPSIZ} = "jpmc15" -o ${JPSIZ} = "jpmc16" -o ${JPSIZ} = "jpdata16" ];then
  OUTPUT_PDF_FILE="${JPSIZ}_h_2d_eff_etaphi_mu4_ALL_${LABEL}.pdf"
  root -l -b "PDFMaker_JPsi.C++(\"$INPUT_ROOT_FILE\",\"$OUTPUT_PDF_FILE\",\"$JPSIZ\",\"eff\",\"2d\",\"etaphi\",\"mu4\",\"ALL\")"
  mv $OUTPUT_PDF_FILE $OUTPUT_PDF_DIR
fi

#Jpsi mu4 2d qetaphi
if [ ${JPSIZ} = "jpmc15" -o ${JPSIZ} = "jpmc16" -o ${JPSIZ} = "jpdata16" ];then
  OUTPUT_PDF_FILE="${JPSIZ}_h_2d_eff_qetaphi_mu4_ALL_${LABEL}.pdf"
  root -l -b "PDFMaker_JPsi.C++(\"$INPUT_ROOT_FILE\",\"$OUTPUT_PDF_FILE\",\"$JPSIZ\",\"eff\",\"2d\",\"qetaphi\",\"mu4\",\"ALL\")"
  mv $OUTPUT_PDF_FILE $OUTPUT_PDF_DIR  
fi

#Jpsi mu4 2d qetapt
if [ ${JPSIZ} = "jpmc15" -o ${JPSIZ} = "jpmc16" -o ${JPSIZ} = "jpdata16" ];then
  OUTPUT_PDF_FILE="${JPSIZ}_h_2d_eff_qetapt_mu4_ALL_${LABEL}.pdf"
  root -l -b "PDFMaker_JPsi.C++(\"$INPUT_ROOT_FILE\",\"$OUTPUT_PDF_FILE\",\"$JPSIZ\",\"eff\",\"2d\",\"qetapt\",\"mu4\",\"ALL\")"
  mv $OUTPUT_PDF_FILE $OUTPUT_PDF_DIR 
fi

####JPsi mu6
#Jpsi mu6 1d pt
if [ ${JPSIZ} = "jpmc15" -o ${JPSIZ} = "jpmc16" -o ${JPSIZ} = "jpdata16" ];then
  OUTPUT_PDF_FILE="${JPSIZ}_h_1d_eff_pt_mu6_ALL_${LABEL}.pdf"
  root -l -b "PDFMaker_JPsi.C++(\"$INPUT_ROOT_FILE\",\"$OUTPUT_PDF_FILE\",\"$JPSIZ\",\"eff\",\"1d\",\"pt\",\"mu6\",\"ALL\")"
  mv $OUTPUT_PDF_FILE $OUTPUT_PDF_DIR  
fi

#Jpsi mu6 1d eta
if [ ${JPSIZ} = "jpmc15" -o ${JPSIZ} = "jpmc16" -o ${JPSIZ} = "jpdata16" ];then
  OUTPUT_PDF_FILE="${JPSIZ}_h_1d_eff_eta_mu6_ALL_${LABEL}.pdf"
  root -l -b "PDFMaker_JPsi.C++(\"$INPUT_ROOT_FILE\",\"$OUTPUT_PDF_FILE\",\"$JPSIZ\",\"eff\",\"1d\",\"eta\",\"mu6\",\"ALL\")"
  mv $OUTPUT_PDF_FILE $OUTPUT_PDF_DIR  
fi

#Jpsi mu6 1d phi
if [ ${JPSIZ} = "jpmc15" -o ${JPSIZ} = "jpmc16" -o ${JPSIZ} = "jpdata16" ];then
  OUTPUT_PDF_FILE="${JPSIZ}_h_1d_eff_phi_mu6_ALL_${LABEL}.pdf"
  root -l -b "PDFMaker_JPsi.C++(\"$INPUT_ROOT_FILE\",\"$OUTPUT_PDF_FILE\",\"$JPSIZ\",\"eff\",\"1d\",\"phi\",\"mu6\",\"ALL\")"
  mv $OUTPUT_PDF_FILE $OUTPUT_PDF_DIR  
fi

#Jpsi mu6 2d etaphi
if [ ${JPSIZ} = "jpmc15" -o ${JPSIZ} = "jpmc16" -o ${JPSIZ} = "jpdata16" ];then
  OUTPUT_PDF_FILE="${JPSIZ}_h_2d_eff_etaphi_mu6_ALL_${LABEL}.pdf"
  root -l -b "PDFMaker_JPsi.C++(\"$INPUT_ROOT_FILE\",\"$OUTPUT_PDF_FILE\",\"$JPSIZ\",\"eff\",\"2d\",\"etaphi\",\"mu6\",\"ALL\")"
  mv $OUTPUT_PDF_FILE $OUTPUT_PDF_DIR
fi

#Jpsi mu6 2d qetaphi
if [ ${JPSIZ} = "jpmc15" -o ${JPSIZ} = "jpmc16" -o ${JPSIZ} = "jpdata16" ];then
  OUTPUT_PDF_FILE="${JPSIZ}_h_2d_eff_qetaphi_mu6_ALL_${LABEL}.pdf"
  root -l -b "PDFMaker_JPsi.C++(\"$INPUT_ROOT_FILE\",\"$OUTPUT_PDF_FILE\",\"$JPSIZ\",\"eff\",\"2d\",\"qetaphi\",\"mu6\",\"ALL\")"
  mv $OUTPUT_PDF_FILE $OUTPUT_PDF_DIR  
fi

#Jpsi mu6 2d qetapt
if [ ${JPSIZ} = "jpmc15" -o ${JPSIZ} = "jpmc16" -o ${JPSIZ} = "jpdata16" ];then
  OUTPUT_PDF_FILE="${JPSIZ}_h_2d_eff_qetapt_mu6_ALL_${LABEL}.pdf"
  root -l -b "PDFMaker_JPsi.C++(\"$INPUT_ROOT_FILE\",\"$OUTPUT_PDF_FILE\",\"$JPSIZ\",\"eff\",\"2d\",\"qetapt\",\"mu6\",\"ALL\")"
  mv $OUTPUT_PDF_FILE $OUTPUT_PDF_DIR 
fi

####Z mu26ivm

#Z 1d pt comparison
if [ ${JPSIZ} = "zmc15" -o ${JPSIZ} = "zdata16" ];then
  OUTPUT_PDF_FILE="${JPSIZ}_h_1d_eff_pt_ALL_compare_${LABEL}.pdf"
  root -l -b "PDFMaker_Z.C++(\"$INPUT_ROOT_FILE\",\"$OUTPUT_PDF_FILE\",\"$JPSIZ\",\"eff\",\"1d\",\"pt\",\"ALL\",\"SELECT\")"
  mv $OUTPUT_PDF_FILE $OUTPUT_PDF_DIR  
fi

#Z mu26ivm 1d pt
if [ ${JPSIZ} = "zmc15" -o ${JPSIZ} = "zdata16" ];then
  OUTPUT_PDF_FILE="${JPSIZ}_h_1d_eff_pt_mu26ivm_ALL_${LABEL}.pdf"
  root -l -b "PDFMaker_Z.C++(\"$INPUT_ROOT_FILE\",\"$OUTPUT_PDF_FILE\",\"$JPSIZ\",\"eff\",\"1d\",\"pt\",\"mu26ivm\",\"ALL\")"
  mv $OUTPUT_PDF_FILE $OUTPUT_PDF_DIR  
fi

#Jpsi mu26ivm 1d eta
if [ ${JPSIZ} = "zmc15" -o ${JPSIZ} = "zdata16" ];then
  OUTPUT_PDF_FILE="${JPSIZ}_h_1d_eff_eta_mu26ivm_ALL_${LABEL}.pdf"
  root -l -b "PDFMaker_Z.C++(\"$INPUT_ROOT_FILE\",\"$OUTPUT_PDF_FILE\",\"$JPSIZ\",\"eff\",\"1d\",\"eta\",\"mu26ivm\",\"ALL\")"
  mv $OUTPUT_PDF_FILE $OUTPUT_PDF_DIR  
fi

#Jpsi mu26ivm 1d phi
if [ ${JPSIZ} = "zmc15" -o ${JPSIZ} = "zdata16" ];then
  OUTPUT_PDF_FILE="${JPSIZ}_h_1d_eff_phi_mu26ivm_ALL_${LABEL}.pdf"
  root -l -b "PDFMaker_Z.C++(\"$INPUT_ROOT_FILE\",\"$OUTPUT_PDF_FILE\",\"$JPSIZ\",\"eff\",\"1d\",\"phi\",\"mu26ivm\",\"ALL\")"
  mv $OUTPUT_PDF_FILE $OUTPUT_PDF_DIR  
fi

#Jpsi mu26ivm 2d etaphi
if [ ${JPSIZ} = "zmc15" -o ${JPSIZ} = "zdata16" ];then
  OUTPUT_PDF_FILE="${JPSIZ}_h_2d_eff_etaphi_mu26ivm_ALL_${LABEL}.pdf"
  root -l -b "PDFMaker_Z.C++(\"$INPUT_ROOT_FILE\",\"$OUTPUT_PDF_FILE\",\"$JPSIZ\",\"eff\",\"2d\",\"etaphi\",\"mu26ivm\",\"ALL\")"
  mv $OUTPUT_PDF_FILE $OUTPUT_PDF_DIR
fi

#Jpsi mu26ivm 2d qetaphi
if [ ${JPSIZ} = "zmc15" -o ${JPSIZ} = "zdata16" ];then
  OUTPUT_PDF_FILE="${JPSIZ}_h_2d_eff_qetaphi_mu26ivm_ALL_${LABEL}.pdf"
  root -l -b "PDFMaker_Z.C++(\"$INPUT_ROOT_FILE\",\"$OUTPUT_PDF_FILE\",\"$JPSIZ\",\"eff\",\"2d\",\"qetaphi\",\"mu26ivm\",\"ALL\")"
  mv $OUTPUT_PDF_FILE $OUTPUT_PDF_DIR  
fi

#Jpsi mu26ivm 2d qetapt
if [ ${JPSIZ} = "zmc15" -o ${JPSIZ} = "zdata16" ];then
  OUTPUT_PDF_FILE="${JPSIZ}_h_2d_eff_qetapt_mu26ivm_ALL_${LABEL}.pdf"
  root -l -b "PDFMaker_Z.C++(\"$INPUT_ROOT_FILE\",\"$OUTPUT_PDF_FILE\",\"$JPSIZ\",\"eff\",\"2d\",\"qetapt\",\"mu26ivm\",\"ALL\")"
  mv $OUTPUT_PDF_FILE $OUTPUT_PDF_DIR 
fi

####Z mu50
#Z mu50 1d pt
if [ ${JPSIZ} = "zmc15" -o ${JPSIZ} = "zdata16" ];then
  OUTPUT_PDF_FILE="${JPSIZ}_h_1d_eff_pt_mu50_ALL_${LABEL}.pdf"
  root -l -b "PDFMaker_Z.C++(\"$INPUT_ROOT_FILE\",\"$OUTPUT_PDF_FILE\",\"$JPSIZ\",\"eff\",\"1d\",\"pt\",\"mu50\",\"ALL\")"
  mv $OUTPUT_PDF_FILE $OUTPUT_PDF_DIR  
fi

#Jpsi mu50 1d eta
if [ ${JPSIZ} = "zmc15" -o ${JPSIZ} = "zdata16" ];then
  OUTPUT_PDF_FILE="${JPSIZ}_h_1d_eff_eta_mu50_ALL_${LABEL}.pdf"
  root -l -b "PDFMaker_Z.C++(\"$INPUT_ROOT_FILE\",\"$OUTPUT_PDF_FILE\",\"$JPSIZ\",\"eff\",\"1d\",\"eta\",\"mu50\",\"ALL\")"
  mv $OUTPUT_PDF_FILE $OUTPUT_PDF_DIR  
fi

#Jpsi mu50 1d phi
if [ ${JPSIZ} = "zmc15" -o ${JPSIZ} = "zdata16" ];then
  OUTPUT_PDF_FILE="${JPSIZ}_h_1d_eff_phi_mu50_ALL_${LABEL}.pdf"
  root -l -b "PDFMaker_Z.C++(\"$INPUT_ROOT_FILE\",\"$OUTPUT_PDF_FILE\",\"$JPSIZ\",\"eff\",\"1d\",\"phi\",\"mu50\",\"ALL\")"
  mv $OUTPUT_PDF_FILE $OUTPUT_PDF_DIR  
fi

#Jpsi mu50 2d etaphi
if [ ${JPSIZ} = "zmc15" -o ${JPSIZ} = "zdata16" ];then
  OUTPUT_PDF_FILE="${JPSIZ}_h_2d_eff_etaphi_mu50_ALL_${LABEL}.pdf"
  root -l -b "PDFMaker_Z.C++(\"$INPUT_ROOT_FILE\",\"$OUTPUT_PDF_FILE\",\"$JPSIZ\",\"eff\",\"2d\",\"etaphi\",\"mu50\",\"ALL\")"
  mv $OUTPUT_PDF_FILE $OUTPUT_PDF_DIR
fi

#Jpsi mu50 2d qetaphi
if [ ${JPSIZ} = "zmc15" -o ${JPSIZ} = "zdata16" ];then
  OUTPUT_PDF_FILE="${JPSIZ}_h_2d_eff_qetaphi_mu50_ALL_${LABEL}.pdf"
  root -l -b "PDFMaker_Z.C++(\"$INPUT_ROOT_FILE\",\"$OUTPUT_PDF_FILE\",\"$JPSIZ\",\"eff\",\"2d\",\"qetaphi\",\"mu50\",\"ALL\")"
  mv $OUTPUT_PDF_FILE $OUTPUT_PDF_DIR  
fi

#Jpsi mu50 2d qetapt
if [ ${JPSIZ} = "zmc15" -o ${JPSIZ} = "zdata16" ];then
  OUTPUT_PDF_FILE="${JPSIZ}_h_2d_eff_qetapt_mu50_ALL_${LABEL}.pdf"
  root -l -b "PDFMaker_Z.C++(\"$INPUT_ROOT_FILE\",\"$OUTPUT_PDF_FILE\",\"$JPSIZ\",\"eff\",\"2d\",\"qetapt\",\"mu50\",\"ALL\")"
  mv $OUTPUT_PDF_FILE $OUTPUT_PDF_DIR 
fi




#evince ${OUTPUT_PDF_DIR}${OUTPUT_PDF_FILE} &

