#! /bin/bash
INPUT_ROOT_FILE=$1
OUTPUT_PDF_DIR="output_${INPUT_ROOT_FILE}/"
VALIABLES_1D_LIST=(mdR mass pt eta phi dR exdR d0 z0)
VALIABLES_2D_LIST=(qetapt qetaphi etaphi phipt dRpt exdRpt exdRdR)
VALIABLES_3D_LIST=(ptetaphi)

HIST_1D_LIST=(hist0,hist1,hist2,hist3,hist4,hist5,hist6,hist7,hsit8)
EFF="eff"
PROBE="probe"
HIST_D1="1d"
HIST_D2="2d"

echo "Base output directory: $OUTPUT_PDF_DIR"

if [ ! -d "$OUTPUT_PDF_DIR" ] ;then
  echo "===============Create ${OUTPUT_PDF_DIR} ===========" && mkdir $OUTPUT_PDF_DIR
fi

echo ""

echo "******************************************************************"
echo "******************************************************************"
echo "[1]dimension,[2]kind,[3]variable,[4]trriger,[5]chain"
echo "******************************************************************"
echo "******************************************************************"

echo ""
echo ""
#step; which jpsi or z
echo "[0]choose \"jpmc16\" or \"jpmc15\" or \"zmc15\" dimension of histgram :"
read JPSIZ
echo ">>>>>>>>>>>>>>>>>>>>>>>"
echo ">>>>>> ${JPSIZ}_ >>>>>>>"
echo ">>>>>>>>>>>>>>>>>>>>>>>"

#1st step; dimension
echo "[1]choose \"1d\" or \"2d\" dimension of histgram :"
read DIM
echo ">>>>>>>>>>>>>>>>>>>>>>>"
echo ">>>>>> h_${DIM}_ >>>>>>>"
echo ">>>>>>>>>>>>>>>>>>>>>>>"

echo ""
#2nd step; kind 
echo "[2]choose \"eff\" or \"probe\"  :"
read KIND 
echo ">>>>>>>>>>>>>>>>>>>>>>>"
echo ">>>>>> h_${DIM}_${KIND}_ >>>>>>"
echo ">>>>>>>>>>>>>>>>>>>>>>>"

echo ""
#3rd step; valiable
if [ "$DIM" = $HIST_D1 ]; then
  echo "[3]choose valiable in \"ALL,mdR,mass,pt,eta,phi,dR,exdR,d0,z0\" :"
  read VARIABLE
echo ">>>>>>>>>>>>>>>>>>>>>>>"
  echo ">>>>> h_${DIM}_${KIND}_${VARIABLE}_ >>>>>>"
  echo ">>>>>>>>>>>>>>>>>>>>>>>"
fi

if [ "$DIM" = $HIST_D2 ]; then
  echo "[3]choose valiable in \"ALL,qetapt,qetaphi,etaphi,phipt,dRpt,exdRpt,exdRdR\" :"
  read VARIABLE
echo ">>>>>>>>>>>>>>>>>>>>>>>"
  echo ">>>>>> h_${DIM}_${KIND}_${VARIABLE}_ >>>>>>"
echo ">>>>>>>>>>>>>>>>>>>>>>>"
fi

echo ""
#4th step; trigger
echo "[4]choose trigger in \"ALL,mu4,mu6,mu6ms,mu10,mu10nc,mu11,mu11nc,mu14,mu18,mu20,mu20ms,mu24im,mu26im,mu26ivm,mu50,mu60,mu4FS,mu4FSMU6,mu6FS,mu6FSMU6,mu8FS,2mu6,2mu10,2mu10nocb,mu11u6\" :"
read TRIGGER 
echo ">>>>>>>>>>>>>>>>>>>>>>>"
echo ">>>>>> h_${DIM}_${KIND}_${VARIABLE}_${TRIGGER}_ >>>>>>"
echo ">>>>>>>>>>>>>>>>>>>>>>>"

echo ""
#5th step; chain
if [ "$KIND" = $EFF ]; then 
  echo "[5]choose chain in \"ALL,L1,L1SA,SACB,L2,L2_2,L2EF,TOTAL,FTF,SAFTF,L1L2,HLT,TOTAL_2,HLT_2\"" 
  read CHAIN
echo ">>>>>>>>>>>>>>>>>>>>>>>"
  echo ">>>>>> h_${DIM}_${KIND}_${VARIABLE}_${TRIGGER}_${CHAIN} >>>>>>"
echo ">>>>>>>>>>>>>>>>>>>>>>>"
fi

if [ "$KIND" = $PROBE ]; then 
  echo "[5]choose chain in \"ALL,PROBE,L1,SA,CB,FTF,FTF_2,PROBEDUMMY,L2DUMMY,EF\""
  read CHAIN
echo ">>>>>>>>>>>>>>>>>>>>>>>"
  echo ">>>>>> h_${DIM}_${KIND}_${VARIABLE}_${TRIGGER}_${CHAIN} >>>>>>"
echo ">>>>>>>>>>>>>>>>>>>>>>>"
fi

#6th step; trigger
echo "[6]choose pdf label :"
read LABEL 
echo ">>>>>>>>>>>>>>>>>>>>>>>"
echo ">>>>>> h_${DIM}_${KIND}_${VARIABLE}_${TRIGGER}_${LABEL} >>>>>>"
echo ">>>>>>>>>>>>>>>>>>>>>>>"


OUTPUT_PDF_FILE="${JPSIZ}_h_${DIM}_${KIND}_${VARIABLE}_${TRIGGER}_${CHAIN}_${LABEL}.pdf"
HIST2="h_${KIND}_eta_${TRIGGER}_${CHAIN}"


i=0
AHIST=()


echo ${AHIST[@]}



if [ ${JPSIZ} = "jpmc15" -o ${JPSIZ} = "jpmc16" ];then
  root -l -b "PDFMaker_JPsi.C++(\"$INPUT_ROOT_FILE\",\"$OUTPUT_PDF_FILE\",\"$JPSIZ\",\"$KIND\",\"$DIM\",\"$VARIABLE\",\"$TRIGGER\",\"$CHAIN\")"
fi

if [ ${JPSIZ} = "zmc15" ];then
  root -l -b "PDFMaker_Z.C++(\"$INPUT_ROOT_FILE\",\"$OUTPUT_PDF_FILE\",\"$JPSIZ\",\"$KIND\",\"$DIM\",\"$VARIABLE\",\"$TRIGGER\",\"$CHAIN\")"
  echo "***********************************************"
  echo "***********************************************"
  echo "***********************************************"
  echo "***********************************************"
  echo "*********************ZZZZ**************************"
  echo "***********************************************"
  echo "***********************************************"
  echo "***********************************************"
  echo "***********************************************"
  echo "***********************************************"
fi
#root -l -b "test.C++(\"$INPUT_ROOT_FILE\",\"$OUTPUT_PDF_FILE\",\"${AHIST[0]}\",\"${AHIST[1]}\",\"${AHIST[2]}\",\"${AHIST[3]}\",\"${AHIST[4]}\",\"${AHIST[5]}\",\"${AHIST[6]}\",\"${AHIST[7]}\",\"${AHIST[8]}\")"


echo "******************************************************************"
echo "*****************************INFORMATION**************************"
echo "******************************************************************"
echo -e  "*** [ input file == $1 ] ****************** \n*** [ dimension == $DIM ] ****************************** \n*** [ output file == $OUTPUT_PDF_FILE ] *****************"
#echo -e  "*** [ input file == $1 ] *** \n*** [ output file == $2 ] *** \n*** [ hist == ${AHIST[0]},${AHIST[1]},${AHIST[2]},${AHIST[3]},${AHIST[4]},${AHIST[5]},${AHIST[6]},${AHIST[7]},${AHIST[8]}] ***"
echo "******************************************************************"
echo "******************************************************************"

mv $OUTPUT_PDF_FILE $OUTPUT_PDF_DIR  

echo $OUTPUT_PDF_DIR$OUTPUT_PDF_FILE

evince ${OUTPUT_PDF_DIR}${OUTPUT_PDF_FILE} &

